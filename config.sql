-- --------------------------------------------------------
-- Host:                         pi-octavia-node
-- Server version:               10.3.23-MariaDB-0+deb10u1 - Raspbian 10
-- Server OS:                    debian-linux-gnueabihf
-- HeidiSQL Version:             11.0.0.6085
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for octavia
DROP DATABASE IF EXISTS `octavia`;
CREATE DATABASE IF NOT EXISTS `octavia` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `octavia`;

-- Dumping structure for table octavia.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `set1` varchar(50) DEFAULT NULL,
  `set2` varchar(50) DEFAULT NULL,
  `set3` varchar(50) DEFAULT NULL,
  `set4` varchar(50) DEFAULT NULL,
  `set5` varchar(50) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table octavia.config: ~7 rows (approximately)
DELETE FROM `config`;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `description`, `set1`, `set2`, `set3`,`set4`,`set5`, `node`, `type`) VALUES
	(1, 'thisnode', 'masterrelay', '#ffffff', NULL,'','', 'masterrelay', NULL),
	(4, 'passcode', '123456', NULL, NULL,NULL,NULL,NULL, NULL),
	(45, 'chat_id', '', NULL, NULL,NULL,NULL, 'masterrelay', 'telegram'),
	(46, 'telegramtoken', '', NULL, NULL,NULL,NULL, 'masterrelay', 'telegram'),
	(61, 'log', 'global', '0', '50','10',NULL, NULL, NULL),
  (61, 'logo', 'assets/octavialogo.png', 'uk-style-light', NULL,NULL,NULL, NULL, NULL)

	
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table octavia.devicetype
DROP TABLE IF EXISTS `devicetype`;
CREATE TABLE IF NOT EXISTS `devicetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table octavia.devicetype: ~3 rows (approximately)
DELETE FROM `devicetype`;
/*!40000 ALTER TABLE `devicetype` DISABLE KEYS */;
INSERT INTO `devicetype` (`id`, `description`, `code`) VALUES
	(1, 'Servo', 0),
	(2, 'Ldd', 1),
	(3, 'Gpio', 2);
/*!40000 ALTER TABLE `devicetype` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

